![Quartier des minimes sous blender](sources/images/minimes.png "Les minimes")

Logiciels utilisés  
Godot 3.1.x  
Blender 2.79.x  
Git  

Récupérer le code du jeu:  
Via git:  
git clone git@framagit.org:godot/thsfx.git  
Via un zip:  
https://framagit.org/godot/thsfx/-/archive/master/thsfx-master.zip  

Lien pour download godot 3.1:  
https://godotengine.org/download/  

Lien pour download blender 2.79:  
https://www.blender.org/download/  

Formats utilisés (libre de droit)  
3d: blend, obj, dae  
images: jpg, png  
son: ogg, wav  

Lien vers addon godot-exporter, à mettre dans le répertoire scripts/addon de blender:  
https://github.com/godotengine/godot-blender-exporter/archive/master.zip  

Pour contribuer, donne-nous ton pseudo framagit pour qu'on t'ajoute au groupe.  

Tutoriel pour texturer avec Blender:  
https://www.youtube.com/watch?v=9PJL0eAuZ_E  
https://www.youtube.com/watch?v=-cgr25rih9I  
https://www.youtube.com/watch?v=Kl_Au5Bc6-g  

Sites pour trouver des textures:  
https://texturehaven.com/  
https://textures.one/  
https://cc0textures.com/  


Tutoriel pour la 3D sous Godot:  
https://www.youtube.com/watch?v=SQ7soQ-N-eQ  
https://www.youtube.com/watch?v=VeCrE-ge8xM  

Pour trouver des objets 3D:  
https://cults3d.com/fr  
https://www.aniwaa.fr/meilleurs-site-telecharger-fichiers-stl-gratuits-modeles-3d-fichiers-imprimables-en-3d/  
https://stock.adobe.com/fr/3d-assets  
http://www.sweethome3d.com/fr/freeModels.jsp  
https://cgtuto.com/33-modeles-darbres-libres-de-droit-a-telecharger/  
